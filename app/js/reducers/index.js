export default function todos(state = [], action) {
	switch (action.type) {
	case 'ADD_USER': 
		return [ action.user, ...state ]
	case 'UPD_USER':
		state.splice(action.id, 1, action.user)
		return state
	case 'DEL_USER':
		state.splice(action.id, 1)
		return state
	default:
		return state
	}
}