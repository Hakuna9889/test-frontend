import React from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { render } from 'react-dom';
import { createStore } from 'redux'
import { Provider } from 'react-redux';
import Reducer from './reducers/index.js'
import Form from './components/form';
import Table from './components/table';
import '../sass/styles.scss';

const store = createStore(Reducer);

render(
	<Provider store={store}>
		<Router>
			<div>
				<Route exact path="/" component={Table} />
				<Route path="/create" component={Form} />
				<Route path="/update/:id" component={Form} />
			</div>
		</Router>
	</Provider>,
	document.getElementById('root'),
);