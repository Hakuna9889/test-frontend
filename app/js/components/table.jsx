import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { connect } from 'react-redux';

class Table extends Component {

	constructor(props) {
		super(props);
	}

	is_image(vars, keys) {
		if ("avatar" === keys) {
			return React.createElement(
				"img",
				{src:vars[keys]}
			)
		} else {
			return vars[keys]
		}
	}

	deleteUserInfo(id) {
		var action = {
			type: 'DEL_USER',
			id: id
		};
		this.props.dispatch(action);
	}

	bust(vars, index) {
		var someArr = [],
			counter = 0
		for (var keys in vars) {
			someArr.push( React.createElement(
				"td",
				{key:counter++},
				this.is_image(vars, keys)
			) )
		}
		someArr.push( 
			<td key={counter++}>
				<button><Link className="update" to={`update/${index}`}>Update</Link></button>
				<button><Link className="delete" to="/" onClick={() => this.deleteUserInfo(index)}>Delete</Link></button>
			</td>
		)
		return someArr
	}

	render() {
		return (
			<div className="app">
				<button><Link className="create" to="/create">Add New</Link></button>
				<table>
					<tbody>
						{
							this.props.store.map(
								(vars, index) => (
									React.createElement(
										"tr",
										{key:index},
										this.bust(vars, index)
									)
								)
							)
						}
					</tbody>
				</table>
			</div>
		)
	}
}

function mapStateToProps (state) {
	return {
		store: state
	}
}

export default connect(mapStateToProps)(Table)