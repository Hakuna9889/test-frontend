import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Data from '../data/users.json';
import { connect } from 'react-redux';

class Form extends Component {
	constructor(props) {
		super(props);
		let output_state_arg = {}
		Data.map( value => {
			if ( props.store[props.match.params.id] ) {
				output_state_arg[value.name] = props.store[props.match.params.id][value.name] ? props.store[props.match.params.id][value.name] : '' 
			} else {
				output_state_arg[value.name] = ''
			}
		})
		this.state = output_state_arg;
		console.log("this", this.state)
		this.saveUsersInfo = this.saveUsersInfo.bind(this);
	}

	saveUsersInfo() {
		var all_inputs = {},
		output_arr = {}

		all_inputs = document.getElementsByClassName("input");
		for (var values in all_inputs) {
			if (all_inputs[values].name && all_inputs[values].value) {
				output_arr[all_inputs[values].name] = all_inputs[values].value;
			}
		}
		if (this.props.store[this.props.match.params.id]) {
			var action = {
				type: 'UPD_USER',
				id: this.props.match.params.id,
				user: output_arr
			};
		} else {
			var action = {
				type: 'ADD_USER',
				user: output_arr
			};
		}
		this.props.dispatch(action);
	}

	bust(row) {
		const handleInput = (e) => {
			const name = e.target.name;
			const value = e.target.value;
			this.setState({[name]: value});
		}
		switch(row.type) {
			case 'select':
				return (
					<select name={row.name} onChange={this.handleInput} value={this.state[row.name]}>
						{row.options.map((varoption, keys) => (React.createElement("option", {value:varoption.value, key:keys}, varoption.text)))}
					</select>
				)
			default: 
				return <input type={row.type} name={row.name} onChange={this.handleInput} value={this.state[row.name]} />
		}
	}

	render() {
		return (
			<div className="app">
				<form action="">
					<table>
						<tbody>{
							Data.map((row, index) => (
								<tr key={index}>
									<td>{row.name}</td>
									<td>
										{this.bust(row)}
									</td>
								</tr> 
							))
						}</tbody>
					</table>
					<button type="submit" onClick={this.saveUsersInfo}><Link className="update" to="/">{this.props.location.pathname == "/create" ? "Add" : "Update"}</Link></button>
				</form>
			</div>
		)
	}
}

function mapStateToProps (state) {
	return {
		store: state
	}
}

export default connect(mapStateToProps)(Form)